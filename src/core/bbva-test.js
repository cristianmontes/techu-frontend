import {PolymerElement} from '@polymer/polymer/polymer-element.js';
import {html} from '@polymer/polymer/lib/utils/html-tag.js';

import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';

class ExampleElement extends PolymerElement {
  static get template() {
    return html`
      <paper-tabs selected="0" scrollable>
        <paper-tab>Tab 0</paper-tab>
        <paper-tab>Tab 1</paper-tab>
        <paper-tab>Tab 2</paper-tab>
        <paper-tab>Tab 3</paper-tab>
      </paper-tabs>
    `;
  }
}

customElements.define('example-element', ExampleElement);