import {PolymerElement, html} from '@polymer/polymer';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icons/social-icons.js';
import '@polymer/app-layout/app-layout.js';
import '@polymer/paper-icon-button/paper-icon-button.js';

import './bbva-drawer.js'
import {Globals} from './globals.js'

class ToolBarElement extends PolymerElement {
  static get template() {
    return html`
      <style>
          app-header {
            display: block;
            background-color: #ffffff;
            color: #000;
            border-top: 6px solid #0066ff;
            border-bottom: 1px solid #80aaff;
          }
          app-header paper-icon-button {
            --paper-icon-button-ink-color: #fff;
          }
      </style>
      <app-header-layout>
        <app-header effects="waterfall" slot="header">
          <app-toolbar>
            
            <div class="navItem leftItem">
              <paper-icon-button id="toggle" icon="menu"></paper-icon-button>
              <paper-icon-button icon=""></paper-icon-button>
            </div>
            <div main-title>
              LOGO
            </div>            
            <div class="navItem">
              <paper-icon-button icon="social:notifications" aria-label="Notificaciones"></paper-icon-button>
              <paper-icon-button icon=""></paper-icon-button>
            </div>
            <div>
              [[usersession.persona.nombre]]
              [[Globals.semillaencriptado]]
            </div>
            <paper-icon-button icon="social:person-outline" aria-label="Datos de la persona"></paper-icon-button>
          </app-toolbar>
        </app-header>
        </app-header-layout>
    `;
  }

  static get properties() {
    return {
      usersession: {
            type: Object
      }
    };
  }
}
customElements.define('toolbar-element', ToolBarElement);